package com.example.bookstore.bookstore.googletest;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONException;
import org.json.JSONObject;

public class CreatepassActivity extends AppCompatActivity {

    DatabaseHelper db;
    Button login, register ,  button;
    EditText password, passwordConf;
    private ImageView circleImageView;
    private TextView txtName,txtEmail;
    private CallbackManager callbackManager;
    ProgressDialog progressDialog;
    private GoogleApiClient googleApiClient;
    GoogleSignInClient mGoogleSignInClient;
    private GoogleSignInOptions gso;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createpass);
        db = new DatabaseHelper(this);

        gso =  new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        password = (EditText)findViewById(R.id.edtText_passwordRegist);
        passwordConf = (EditText)findViewById(R.id.edtText_passwordConfRegist);
        button = findViewById(R.id.pass_signout);
        txtName = findViewById(R.id.create_text_name);
        googleApiClient=new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
//        get email and create new password. then  save in sqllite
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    // ...
                    case R.id.pass_signout:
                        String strUsername = txtEmail.getText().toString();
                        String strPassword = password.getText().toString();
                        String strName = txtName.getText().toString();
                        String strPasswordConf = passwordConf.getText().toString();

                        if (!strPassword.equals(strPasswordConf))
                        {
                            //Popup message if passwords don't match
                            Toast pass = Toast.makeText(CreatepassActivity.this, "Hmmm...Passwords didn't match!", Toast.LENGTH_SHORT);
                            pass.show();
                        }

                        else
                        {
                            //Insert details in the database
                            contact contact = new contact();
                            contact.setName(strName);
                            contact.setEmail(strUsername);
                            contact.setPass(strPassword);

                            db.insertContact(contact);
                            Intent intent = new Intent(CreatepassActivity.this,LoginActivity.class);
                            intent.putExtra("Name",strName);
                            startActivity(intent);
                            finish();
                        }
                        break;
                    // ...
                }
            }
        });
//        txtName = findViewById(R.id.create_txt_birthday);
        txtEmail = findViewById(R.id.create_text_email);
//        circleImageView = findViewById(R.id.create_avatar);

        callbackManager = CallbackManager.Factory.create();
//       check facebook login
        checkLoginStatus();

        GoogleSignInAccount acct = GoogleSignIn.getLastSignedInAccount(this);
        if (acct != null) {
            String personName = acct.getDisplayName();
            String personEmail = acct.getEmail();
            String personId = acct.getId();
            Uri personPhoto = acct.getPhotoUrl();

            txtName.setText(personName);
            txtEmail.setText(personEmail);

//            Glide.with(this).load(String.valueOf(personPhoto)).into(circleImageView);

        }

        callbackManager = CallbackManager.Factory.create();
        checkLoginStatus();
    }

    private void checkLoginStatus()
    {
        if(AccessToken.getCurrentAccessToken()!=null)
        {
            loadUserProfile(AccessToken.getCurrentAccessToken());
        }
    }
    private void loadUserProfile(AccessToken newAccessToken)
    {
        GraphRequest request = GraphRequest.newMeRequest(newAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response)
            {
                try {
                    String first_name = object.getString("first_name");
                    String last_name = object.getString("last_name");
//                    String email = object.getString("phone");
                    String id = object.getString("id");
                    String image_url = "https://graph.facebook.com/"+id+ "/picture?type=normal";

//                    txtEmail.setText(email);
                    txtName.setText(first_name +" "+last_name);
                    String full_name = txtName.getText().toString();
                    RequestOptions requestOptions = new RequestOptions();
                    requestOptions.dontAnimate();

                    contact contact = new contact();
                    contact.setName(full_name);
                    Intent intent = new Intent(CreatepassActivity.this,MainActivity.class);
                    intent.putExtra("personName",first_name);

//                    Glide.with(CreatPassActivity.this).load(image_url).into(circleImageView);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields","first_name,last_name,email,id");
        request.setParameters(parameters);
        request.executeAsync();

    }
    AccessTokenTracker tokenTracker = new AccessTokenTracker() {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken)
        {
            if(currentAccessToken==null)
            {
//                txtName.setText("");
                txtEmail.setText("");
//                circleImageView.setImageResource(0);
                Toast.makeText(CreatepassActivity.this,"User Logged out",Toast.LENGTH_LONG).show();
            }
            else
                loadUserProfile(currentAccessToken);
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }
    @Override
    protected void onStop() {
        super.onStop();

        // Disconnect GoogleApiClient when stopping Activity
        googleApiClient.disconnect();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                    }
                });
        Intent intent = new Intent(CreatepassActivity.this,SignupActivity.class);
        startActivity( intent);
        finish();
    }
}
